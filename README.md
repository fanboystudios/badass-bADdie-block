# Welcome to badass bADdieblock!

**The Complete Blacklist For Blocking Bad Things Online Including: Advertisements, Malware, Spyware, Scams, Porn, and Other Undesirable Content!**

_**Maintained by Fanboy Studios / Random Fandom Media Group (https://randomfandom.cf)**_


This is badass bADdieblock, and as such it is a very fucking large blocklist! (The file itself is over 100 MB, and can crash many notepad programs.)
It contains over 6,000,000+ bADdies (ads, malware, spyware, scams, porn, etc.) as opposed to the legacy versions 500,000+ bADdies.
This list has not been tested on an original Raspberry Pi, but it crashes the Google Cloud Free Tier compute engine which is about as powerful as a RPi. 
Do not run this on a weak machine or it may crash or be very unreliable. (I estimate you will need a minimum of 1 GB of RAM.)


_I intend for this file to block a lot of stuff, so there may be a ton of false positive blocked sites._
_If you would like for me to unblock them, add a comment! If I get enough requests, I'll then whitelist the site...whithin reason of course._
_Certain commonly whitelisted sites may be whitelisted._



# FAQs (Frequently Asked Questions, or whatever I might think of that would be asked a ton...)
**Q: My browser tab crashes when trying to view the file. How do I get the raw link or download the file?**

_A: The links are listed below. You can use the raw link in PiHole._

_Raw link: https://gitlab.com/fanboystudios/badass-bADdie-block/-/raw/master/badass%20bADdieblock.txt_

_Download link: https://gitlab.com/fanboystudios/badass-bADdie-block/-/raw/master/badass%20bADdieblock.txt?inline=false_

_Repo link: https://gitlab.com/fanboystudios/badass-bADdie-block/_


**Q: The list isn't working. How do I make it work?**

_A: A list of possible issues are listed below._

_This list is not in hosts file format, as it lacks the ip address redirections at the beginning of each domain. This won't work as a normal system hosts file, it's just a list of domains._

_To make badass bADdieblock work as a system hosts file, append 0.0.0.0 (may be faster) or 127.0.0.1 (more compatible) at the beginning of each domain. (Use regex and it'll be much faster.)_

_Your device might be too weak, or the software you use to manage the blocklist implementaion might not work well with large files._


**Q: How do I edit this file without crashing my PC or software?**

_A: Some good software and some tips are listed below. This is not a comprehensive list._

_I recommend using Notepad++ and EditPad Lite/Pro on Windows. Geany or Gedit on Linux/Mac. You will need a fairly powerful machine for editing this file._

_I also recommend researching/learning regex commands (for quick bullk editing), using punycode (for internationalized domains), and of course keeping backups/saving your work often._


**Q: How should you contact me for Whitelisting Requests?**

_A:	Open an issue on this repo, with detailed reasons why I should whitelist the domain._


# Disclaimer:
This product is provided as-is with no warranties or liablities. I am not responsible for anything happening from your use of this file; not limited to but including: dying kittens, lost data, bricked devices, nuclear war, or the zombie appocalypse.



# License:  
This project is licensed under the IDGAF Public Domain License. A copy of this license is included in this repo.
https://gitlab.com/fanboystudios/the-idgaf-public-domain-license/-/blob/main/LICENSE



# Thank You for using badass bADdieblock!
You are making the internet a prettier and safer place!
